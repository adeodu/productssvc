import { Component, OnInit, Input, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';

@Component({
  selector: 'app-grid-cell',
  // templateUrl: './grid-cell.component.html',
  template: '',
  styleUrls: ['./grid-cell.component.css']
})
export class GridCellComponent implements OnInit {

  @Input() componentType: any;

  constructor(private viewContainerRef: ViewContainerRef,
              private cfr: ComponentFactoryResolver) {
  }

  ngOnInit() {
      let compFactory = this.cfr.resolveComponentFactory(this.componentType);
      this.viewContainerRef.createComponent(compFactory);
  }

}

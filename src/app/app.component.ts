import { Component, Input } from '@angular/core';
import { BlueDynamicComponent } from './blue-dynamic/blue-dynamic.component';
import { GreenDynamicComponent } from './green-dynamic/green-dynamic.component';
import { RedDynamicComponent } from './red-dynamic/red-dynamic.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';

  @Input() componentTypes: any[] = [BlueDynamicComponent, GreenDynamicComponent, RedDynamicComponent];
  @Input() selectedComponentType: any;

  ngOnInit() {
    // default to the first available option
    this.selectedComponentType = this.componentTypes ? this.componentTypes[0] : null;
    
    console.log('ProductslistUI: localStorage auth_key is: ' + window.localStorage.getItem('auth_key-homepage'));
    console.log('ProductslistUI: sessionStorage auth_key is: ' + window.sessionStorage.getItem('auth_key-homepage'));
  }

}

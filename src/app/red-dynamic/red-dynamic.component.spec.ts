import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedDynamicComponent } from './red-dynamic.component';

describe('RedDynamicComponent', () => {
  let component: RedDynamicComponent;
  let fixture: ComponentFixture<RedDynamicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedDynamicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedDynamicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

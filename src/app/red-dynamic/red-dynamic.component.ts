import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-red-dynamic',
  // templateUrl: './red-dynamic.component.html',
  template: '<div class="img-rounded" style="background-color: red;margin: 5px"> Red Dynamic Component! </div>',
  styleUrls: ['./red-dynamic.component.css']
})
export class RedDynamicComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
// All this component does is display a little styled text.
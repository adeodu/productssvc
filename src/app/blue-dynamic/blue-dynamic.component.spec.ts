import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlueDynamicComponent } from './blue-dynamic.component';

describe('BlueDynamicComponent', () => {
  let component: BlueDynamicComponent;
  let fixture: ComponentFixture<BlueDynamicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlueDynamicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlueDynamicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

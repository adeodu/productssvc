import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blue-dynamic',
  // templateUrl: './blue-dynamic.component.html',
  template: '<div class="img-rounded" style="background-color: lightskyblue;margin: 5px"> Blue Dynamic Component! </div>',
  styleUrls: ['./blue-dynamic.component.css']
})
export class BlueDynamicComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
// All this component does is display a little styled text.
// http://angularjs.blogspot.com/2017/01/understanding-aot-and-dynamic-components.html
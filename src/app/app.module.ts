import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { GridModule } from './gridfiles/grid.module';
import { AlertModule } from 'ng2-bootstrap';
// import { APP_BASE_HREF } from '@angular/common';

import { AppComponent } from './app.component';
import { AlertComponent } from './alert/alert.component';
import { AppHeaderComponent } from './app-header/app-header.component';
import { BlueDynamicComponent } from './blue-dynamic/blue-dynamic.component';
import { GreenDynamicComponent } from './green-dynamic/green-dynamic.component';
import { RedDynamicComponent } from './red-dynamic/red-dynamic.component';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    AppHeaderComponent,
    BlueDynamicComponent,
    GreenDynamicComponent,
    RedDynamicComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AlertModule,
    // We declare our Components in the usual way, but we additionally need to register them with the Library 
    // by adding them to the Library's NgModule.entryComponent entry as below
    GridModule.withComponents([
        BlueDynamicComponent,
        GreenDynamicComponent,
        RedDynamicComponent
    ])
  ],
  providers: [],
  // providers: [{provide: APP_BASE_HREF, useValue : '/msportal/products/' }],
  entryComponents: [ AlertComponent ], //we need to tell Angular's AOT Compiler to create factories for the user provided Components, or ComponentFactoryResolver won't find them
  bootstrap: [AppComponent]
})
export class AppModule { }

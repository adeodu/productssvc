import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-alert',
  // templateUrl: './alert.component.html',
  // template: `
  //   <h1>Alert: {{type}}</h1>
  // `,
  template: `
    <h3 (click)="output.next('output')">Alert: {{type}}</h3>
  `,
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  @Input() type: string = "Successful!";
  @Output() output = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

}

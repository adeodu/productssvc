import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-grid',
  // templateUrl: './grid.component.html',
  template: `
      <div class="row" *ngFor="let cellComponentType of cellComponentTypes">
          <div class="col-lg-12">
              <app-grid-cell [componentType]="cellComponentType"></app-grid-cell>
          </div>
      </div>
  `,
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit {

  @Input() componentTypes: any;

  cellComponentTypes: any[] = [];

  addDynamicCellComponent(selectedComponentType:any) {
      this.cellComponentTypes.push(selectedComponentType);
  }

  constructor() { }

  ngOnInit() {
  }

}

// You'll notice that we don't have a template here - that's deliberate as the Cell doesn't 
// have any content of its own - all it does is serve up the user supplied Component

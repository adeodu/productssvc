import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GreenDynamicComponent } from './green-dynamic.component';

describe('GreenDynamicComponent', () => {
  let component: GreenDynamicComponent;
  let fixture: ComponentFixture<GreenDynamicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GreenDynamicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GreenDynamicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

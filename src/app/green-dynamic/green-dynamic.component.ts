import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-green-dynamic',
  // templateUrl: './green-dynamic.component.html',
  template: '<div class="img-rounded" style="background-color: green;margin: 5px"> Green Dynamic Component! </div>',
  styleUrls: ['./green-dynamic.component.css']
})
export class GreenDynamicComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
// All this component does is display a little styled text.
import { Component, ViewChild, ViewContainerRef, 
  ComponentFactoryResolver, ComponentFactory, ComponentRef,
  Input, ChangeDetectorRef, TemplateRef, Output, EventEmitter, Injector  } from '@angular/core';
import { AlertComponent } from '../alert/alert.component';

@Component({
  selector: 'app-app-header',
  // templateUrl: './app-header.component.html',
  template: `
    <ng-container #alertContainer></ng-container>
    <button (click)="createComponent('Header 1')">Create Header 1</button>
    <button (click)="createComponent('Header 2')">Create Header 2</button>
  `,
  styleUrls: ['./app-header.component.css']
})
export class AppHeaderComponent {

  @ViewChild("alertContainer", { read: ViewContainerRef }) container: ViewContainerRef;
  componentRef: ComponentRef<AlertComponent>;
  factory: ComponentFactory<AlertComponent>;
  
  constructor(private injector: Injector, private resolver: ComponentFactoryResolver) { }

  createComponent(type) {
    this.container.clear();
    this.factory = this.resolver.resolveComponentFactory(AlertComponent);
    this.componentRef = this.factory.create(this.injector);
    let view = this.componentRef.hostView;

    this.componentRef = this.container.createComponent(this.factory);    
    this.componentRef.instance.type = type;
    this.componentRef.instance.output.subscribe(event => console.log(event));

  }
  
  ngOnDestroy() {
    this.componentRef.destroy();    
  }

}

// https://netbasal.com/dynamically-creating-components-with-angular-a7346f4a982d